#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobjectArray JNICALL Java_library_minorMatrix_1
  (JNIEnv *env, jobject object, jint deleteRow, jint deleteColumn, jobjectArray matrix, jint matrixRows, jint matrixColumns)
{
    javaEnv = env;
    int c_deleteRow = toInt(deleteRow);
    int c_deleteColumn = toInt(deleteColumn);
    int c_matrixRows = toInt(matrixRows);
    int c_matrixColumns = toInt(matrixColumns);
    int** c_matrix = toIntMatrix(matrix, c_matrixRows, c_matrixColumns);
    int c_minorMatrixRows = c_matrixRows-1;
    int c_minorMatrixColumns = c_matrixColumns-1;
    int** c_outValue = minorMatrix(c_deleteRow, c_deleteColumn, c_matrix, c_matrixRows, c_matrixColumns);
    setJintMatrix(matrix , c_matrix, &c_matrixRows, &c_matrixColumns);
    if (c_outValue == NULL) {
        return NULL;
    } else {
        return toJintMatrix(c_outValue, &c_minorMatrixRows, &c_minorMatrixColumns);
    }
}

JNIEXPORT jint JNICALL Java_library_determinant_1
  (JNIEnv *env, jobject object, jobjectArray matrix, jint matrixRows, jint matrixColumns)
{
    javaEnv = env;
    int c_matrixRows = toInt(matrixRows);
    int c_matrixColumns = toInt(matrixColumns);
    int** c_matrix = toIntMatrix(matrix, c_matrixRows, c_matrixColumns);
    int c_outValue = determinant(c_matrix, c_matrixRows, c_matrixColumns);
    setJintMatrix(matrix , c_matrix, &c_matrixRows, &c_matrixColumns);
    return toJint(c_outValue);
}
