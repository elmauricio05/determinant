#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testMinorMatrix_A() {
    // Given
    int** matrix = (int**) malloc(2 * sizeof(int*));
		matrix[0] = (int*) malloc(2 * sizeof(int));
			matrix[0][0] = -6;
			matrix[0][1] = -15;
		matrix[1] = (int*) malloc(2 * sizeof(int));
    		matrix[1][0] = 79;
    		matrix[1][1] = -9;
    
    int** esperada = (int**) malloc(1 * sizeof(int*));
		esperada[0] = (int*) malloc(1 * sizeof(int));
		esperada[0][0] = 79;
    // When
    int** resultado = minorMatrix(0,1, matrix, 2, 2);
    // Then
       int** solucion = (int**) malloc(1 * sizeof(int*));
       	   solucion[0] = (int*) malloc(2 * sizeof(int));
       	   solucion[0][0] = 79;
       assertMatrixEquals_int(solucion, 1, 1, resultado,1 ,1);
}

void testMinorMatrix_B() {
   // Given
    int** matrix = (int**) malloc(2 * sizeof(int*));
		matrix[0] = (int*) malloc(2 * sizeof(int));
			matrix[0][0] = -6;
			matrix[0][1] = -15;
		matrix[1] = (int*) malloc(2 * sizeof(int));
			matrix[1][0] = 79;
			matrix[1][1] = -9;
    
    int** esperada = (int**) malloc(1 * sizeof(int*));
		esperada[0] = (int*) malloc(1 * sizeof(int));
			esperada[0][0] = -15;
    // When
    int** resultado = minorMatrix(1,0, matrix, 2, 2);
    // Then
       int** solucion = (int**) malloc(1 * sizeof(int*));
       	   solucion[0] = (int*) malloc(2 * sizeof(int));
       	   solucion[0][0] = -15;
       assertMatrixEquals_int(solucion, 1, 1, resultado,1 ,1);
}

void testMinorMatrix_C() {
    // Given
    int** matrix = (int**) malloc(3 * sizeof(int*));
		matrix[0] = (int*) malloc(3 * sizeof(int));
			matrix[0][0] = -2;
			matrix[0][1] = -20;
			matrix[0][2] = 13;
		matrix[1] = (int*) malloc(3 * sizeof(int));
			matrix[1][0] = 3;
			matrix[1][1] = -1;
			matrix[1][2] = -18;
		matrix[2] = (int*) malloc(3 * sizeof(int));
			matrix[2][0] = 14;
			matrix[2][1] = 4;
			matrix[2][2] = 0;
    
    int** esperada = (int**) malloc(2 * sizeof(int*));
		esperada[0] = (int*) malloc(2 * sizeof(int));
			esperada[0][0] = -2;
			esperada[0][1] = 13;
		esperada[1] = (int*) malloc(2 * sizeof(int));
			esperada[1][0] = 3;
			esperada[1][1] = -18;
    // When
    int** resultado = minorMatrix(2,1, matrix, 3, 3);
    // Then
     int** solucion = (int**) malloc(2 * sizeof(int*));
       solucion[0] = (int*) malloc(2 * sizeof(int));
       	   solucion[0][0] = -2;
       	   solucion[0][1] = 13;
       solucion[1] = (int*) malloc(2 * sizeof(int));
       	   solucion[1][0] = 3;
       	   solucion[1][1] = -18;
    assertMatrixEquals_int(solucion, 2, 2, resultado,2 ,2);
}

void testMinorMatrix_D() {
    // Given
    int** matrix = (int**) malloc(3 * sizeof(int*));
		matrix[0] = (int*) malloc(3 * sizeof(int));
			matrix[0][0] = -2;
			matrix[0][1] = -20;
			matrix[0][2] = 13;
		matrix[1] = (int*) malloc(3 * sizeof(int));
			matrix[1][0] = 3;
			matrix[1][1] = -1;
			matrix[1][2] = -18;
		matrix[2] = (int*) malloc(3 * sizeof(int));
		matrix[2][0] = 14;
		matrix[2][1] = 4;
		matrix[2][2] = 0;
    
    int** esperada = (int**) malloc(2 * sizeof(int*));
		esperada[0] = (int*) malloc(2 * sizeof(int));
			esperada[0][0] = 3;
			esperada[0][1] = -18;
		esperada[1] = (int*) malloc(2 * sizeof(int));
			esperada[1][0] = 14;
    esperada[1][1] = 0;
    // When
    int** resultado = minorMatrix(1,0, matrix, 3, 3);
    // Then
    int** solucion = (int**) malloc(2 * sizeof(int*));
		solucion[0] = (int*) malloc(2 * sizeof(int));
			solucion[0][0] = 3;
			solucion[0][1] = -18;
		solucion[1] = (int*) malloc(2 * sizeof(int));
			solucion[1][0] = 14;
			solucion[1][1] = 0;
    assertMatrixEquals_int(solucion, 2, 2, resultado,2 ,2);
}

void testMinorMatrix_E() {
    // Given
    int** matrix = (int**) malloc(4 * sizeof(int*));
	matrix[0] = (int*) malloc(4 * sizeof(int));
    	matrix[0][0] = 5;
    	matrix[0][1] = 6;
    	matrix[0][2] = -16;
    	matrix[0][3] = 16;
    matrix[1] = (int*) malloc(4 * sizeof(int));
    	matrix[1][0] = 7;
    	matrix[1][1] = 8;
    	matrix[1][2] = -14;
    	matrix[1][3] = 17;
    matrix[2] = (int*) malloc(4 * sizeof(int));
    	matrix[2][0] = 9;
    	matrix[2][1] = 10;
    	matrix[2][2] = -13;
    	matrix[2][3] = 18;
    matrix[3] = (int*) malloc(4 * sizeof(int));
    	matrix[3][0] = 11;
    	matrix[3][1] = 19;
    	matrix[3][2] = -12;
    	matrix[3][3] = 20;
    
    int** esperada = (int**) malloc(3 * sizeof(int*));
	esperada[0] = (int*) malloc(3 * sizeof(int));
    	esperada[0][0] = 5;
    	esperada[0][1] = -16;
    	esperada[0][2] = 16;
    	esperada[1] = (int*) malloc(3 * sizeof(int));
    	esperada[1][0] = 7;
    	esperada[1][1] = -14;
    	esperada[1][2] = 17;
    esperada[2] = (int*) malloc(3 * sizeof(int));
    	esperada[2][0] = 11;
    	esperada[2][1] = -12;
    	esperada[2][2] = 20;
    // When
    int** resultado = minorMatrix(2,1, matrix, 4, 4);
    // Then
       int** solucion = (int**) malloc(3 * sizeof(int*));
	solucion[0] = (int*) malloc(3 * sizeof(int));
    	solucion[0][0] = 5;
    	solucion[0][1] = -16;
    	solucion[0][2] = 16;
    solucion[1] = (int*) malloc(3 * sizeof(int));
    	solucion[1][0] = 7;
    	solucion[1][1] = -14;
    	solucion[1][2] = 17;
    solucion[2] = (int*) malloc(3 * sizeof(int));
    	solucion[2][0] = 11;
    	solucion[2][1] = -12;
    	solucion[2][2] = 20;
    assertMatrixEquals_int(solucion, 3, 3, resultado,3 ,3);
}

void testDeterminant_A() {
    // Given
    int** determinante = (int**) malloc(1 * sizeof(int*));
		determinante[0] = (int*) malloc(2 * sizeof(int));
		determinante[0][0] = 62;
		determinante[0][1] = -19;
    int** esperado = (int**) malloc(1 * sizeof(int*));
		esperado[0] = (int*) malloc(1 * sizeof(int));
		esperado[0][0] = 0;
    // When
    int** resultado = determinant(determinante, 0,1);
    // Then
       int** solucion = (int**) malloc(1 * sizeof(int*));
       	   solucion[0] = (int*) malloc(2 * sizeof(int));
       	   solucion[0][0] = 0;
    assertMatrixEquals_int(solucion, 1, 1, resultado,1 ,1);
}

void testDeterminant_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
//
